package com.example.testview;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Chronometer;

public class PongActivity extends Activity {
    public int winScore=0;
    int score = 0;
    @Override
    public void onBackPressed() {
        final Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("score", score);
        startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new MyView(this));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            winScore = Integer.parseInt(extras.getString("winScore"));

        }
    }
    public class MyChronometer extends Chronometer {

        public int msElapsed;
        public boolean isRunning = false;

        public MyChronometer(Context context) {
            super(context);
        }

        @Override
        public void start() {
            super.start();
            setBase(SystemClock.elapsedRealtime() - msElapsed);
            isRunning = true;
        }

        @Override
        public void stop() {
            super.stop();
            if(isRunning) {
                msElapsed = (int)(SystemClock.elapsedRealtime() - this.getBase());
            }
            isRunning = false;
        }
    }

    public class MyView extends View {

        public float x0 ;
        public float y0;
        public float x1 ;
        public float x2 ;
        public float xSpd;
        public float ySpd;
        public float halfHeight;

        float w = 200;
        float h = 25;
        float playerBox[][] = new float[2][4];
        float ballBox[][] = new float[1][4];
        public int gameState=0;
        public int countDown=3;
        MyChronometer chrono;

        public MyView(Context context) {
            super(context);
            chrono=new MyChronometer(context);
            setFocusableInTouchMode(true);
        }
        @Override
        public void onWindowFocusChanged(boolean hasFocus) {
            super.onWindowFocusChanged(hasFocus);
            //init variables
            centerBall();
            x1 = (float) getWidth() / 2;
            x2 = (float) getWidth() / 2;
            xSpd = 10;
            ySpd = 10;
            halfHeight=getHeight()/2;

        }
        //game instances
        public player p1=new player();
        public player p2=new player();
        public Ball ball=new Ball();
        @Override
        protected void onDraw(Canvas canvas) {
            ///MAIN CONTROL LOOP AND DRAWING HERE...
            super.onDraw(canvas);
            canvas.drawColor(Color.BLACK);//draw BG
            //p1 box
            playerBox[0][0]=0 + x1-w/2;
            playerBox[0][1]=h - h/2+halfHeight/6;
            playerBox[0][2]=w/2 + x1;
            playerBox[0][3]=h+ h/2+halfHeight/6;
            //p2 box
            playerBox[1][0]=0 + x2-w/2;
            playerBox[1][1]=getHeight() - h/2-h-halfHeight/6;
            playerBox[1][2]=w/2 + x2;
            playerBox[1][3]=getHeight() + h/2-h-halfHeight/6;

            //ball box
            ballBox[0][0] =0 + x0 - h / 2;
            ballBox[0][1] =y0 - h / 2;
            ballBox[0][2] =h / 2 + x0;
            ballBox[0][3] =y0 + h / 2;
            //draw boxes
            canvas.drawRect(playerBox[0][0],playerBox[0][1] ,playerBox[0][2] ,playerBox[0][3] , p1.paint);//draw p1
            canvas.drawRect(playerBox[1][0],playerBox[1][1] ,playerBox[1][2] ,playerBox[1][3] , p2.paint); //draw p2
            canvas.drawRect(ballBox[0][0],ballBox[0][1] ,ballBox[0][2] ,ballBox[0][3] , ball.paint);  //draw ball
            //controls

            if(gameState==0){

                //game over
                if ((p1.score >=winScore)|| (p2.score>=winScore)){
                    gameState=4;
                }else
                {
                    chrono.start();
                    (chrono).setBase(SystemClock.elapsedRealtime());
                    gameState=1;
                }
            }
            if(gameState==1){
                countDown= (int) (0.002*(SystemClock.elapsedRealtime()-chrono.getBase()));
                drawCountdown(canvas, 3-countDown);

                if(3-countDown==0){
                gameState=2;
                }
            }
            if(gameState==2){

                chrono.stop();
                gameState=3;
            }
            if(gameState==3)
                controlCollision();//also sets the score
            if(gameState==4){
                if(p1.score >=winScore)
                    displayWinner(canvas,"Player 1");
                if (p2.score>=winScore)
                    displayWinner(canvas,"Player 2");
            }

            drawWhiteLine(canvas);//draw white line
            drawScore(canvas);//draw score
            invalidate();////refresh screen:
        }


        //players moves
        private int mActivePointerId;
        @Override
        public boolean onTouchEvent(MotionEvent event) {
            for (int i = 0; i < event.getPointerCount(); i++) {
                // Get the pointer ID
                mActivePointerId = event.getPointerId(i);
                // Use the pointer ID to find the index of the active pointer
                // and fetch its position
                int pointerIndex = event.findPointerIndex(mActivePointerId);
                // Get the pointer's current position
                int x = (int) event.getX(pointerIndex);
                int y = (int) event.getY(pointerIndex);
                if (y < halfHeight)
                    x1 =  lerp( x1, (float)  x,  (float) 0.3) ;
                else
                    x2 =  lerp( x2, (float)  x,  (float) 0.3) ;
            }
            return true;
        }
        float lerp(float a, float b, float f)
        {
            return a + f * (b - a);
        }
        private void controlCollision() {
            //players - for loop for better detection (in between position and planned position places...we could do better)
            //p1

            for (int i=0;i>ySpd;i-=h){
            if (((y0 >playerBox[0][1]+i)
            &&(y0<playerBox[0][3]+i))
            &&(x0>playerBox[0][0])
            &&(x0<playerBox[0][2])) {
                onBallCollide();
                break;
                }
            }
            //p2
            for (int i=0;i<ySpd;i+=h){
                if (((y0 >playerBox[1][1]+i)
                &&(y0<playerBox[1][3]+i))
                &&(x0>playerBox[1][0])
                &&(x0<playerBox[1][2]))  {
                onBallCollide();
                break;
                }
            }
            //walls : reflection
            if ((x0 +xSpd<= 0) ) {
                xSpd = -xSpd;
                while (x0 +xSpd<= 0){
                    x0+=1;
                }
            }else
            if ((x0+xSpd >= getWidth())) {
                xSpd = -xSpd;
                while (x0+xSpd>= getWidth()){
                    x0-=1;
                }
            }else
            //top/bottom : score points and start over (ball at center)
            if (y0 <= 0){
                scorePoint(p2);
                setHiscore();
                centerBall();
                gameState=0;
            }else
            if (y0 >= getHeight()){
                scorePoint(p1);
                setHiscore();
                centerBall();
                gameState=0;
            }else{
                moveBall(); //ball speed
            }
        }
        private void moveBall() {
            //simple ball move
            x0 += xSpd;
            y0 += ySpd;
        }
        private void drawCountdown(Canvas canvas,int countdown) {
            //draws score
            Paint paint = new Paint();
            paint.setTextSize(50*countdown);
            paint.setColor(Color.WHITE);
            canvas.drawText(String.valueOf(countdown), getWidth()/2, halfHeight, paint);

        }
        private void drawScore(Canvas canvas) {
            //draws score
            Paint paint = new Paint();
            paint.setTextSize(50);
            paint.setColor(Color.WHITE);
            canvas.drawText(String.valueOf(p1.score), 10, halfHeight-100, paint);
            canvas.drawText(String.valueOf(p2.score), 10, halfHeight+100, paint);
        }
        private void displayWinner(Canvas canvas,String str) {
            //draws score
            Paint paint = new Paint();
            paint.setTextSize(50);
            paint.setColor(Color.WHITE);
            canvas.drawText(String.valueOf(str)+" wins the match!", getWidth()/8, halfHeight+128, paint);
        }


        private void drawWhiteLine(Canvas canvas) {
            //white line at the center of the screen
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            int i = 0;
            while (i <getWidth()){
                canvas.drawRect(0 + i - h / 2, halfHeight - h / 2, h / 2 + i, halfHeight+ h / 2, paint);
                i+=2*h;
            }
        }
        private void centerBall() {
            //ball at center
            x0 = (float) getWidth() / 2;
            y0 = (float) getHeight() / 2;
        }
        private void setHiscore() {
            //choose higher score
            if (p1.score>p2.score)
                score=p1.score;
            else
                score=p2.score;
        }
        private void scorePoint(player p) {
            //add point and change ball direction
            ySpd = -ySpd;
            p.score+=1;
        }
        private void onBallCollide() {
            //speed change and increase
            ySpd = (float)( -1*ySpd*1.01);
            xSpd+=.01*(x0-x2);//todo : bug
        }
    }
}
