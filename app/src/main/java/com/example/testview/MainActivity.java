package com.example.testview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    //todo : idées
    //mon petit momomo
    //des idées si tu te fais chier :
    // lvl 0 : faire un bouton exit
    // lvl 0.5 : le son
    // lvl 1 : activité pong 4 joueur
    // lvl 2 : casse brique arkanoid
    // lvl 3 : le 1er joueur rattrape les balles  que le 2eme joueur envoie avec son doigt dans la direction du mouvement
    int score=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final Intent intent = new Intent(this, PongActivity.class);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            score = extras.getInt ("score");
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button button = findViewById(R.id.button);
        final TextView textView = findViewById(R.id.textScore);
        final EditText winScore = findViewById(R.id.textWin);
        textView.setText(( String.valueOf(score)));

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                intent.putExtra("winScore", winScore.getText().toString());
                startActivity(intent);

            }
        });
    }
}
